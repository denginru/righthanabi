﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace dengin_roman
{
	public class Hanabi
	{
		private static readonly int  cardsCountPerPlayer = 5;
        private static readonly int maxRank = 5;
		private enum Сolors { Red, Green, Blue, Yellow, White };
        public enum turnResult { successPlayedCard, riskSuccess, newGame, failedRisk, other, ignoreTurn };
        private static Stack<Card> Deck;
		private int TotalTurns;
		private int TotoalCards;
		private int SuccesRiskCards;
		private static List<Card> CurrentSet;

		public bool Finished
		{
			get;
			private set;
		}

		public bool WasStatGet
		{
			get;
			private set;
		}

		private class Card
		{
            private List<Сolors> PossibleColors;
            private List<int> PossibleRanks;

            public Сolors Color { get; private set; }
			public int Rank { get; private set; }
			public bool ColorIsKnown { get; private set; }
			public bool RankIsKnown { get; private set; }

			public Card(Сolors color, int rank)
			{
				Color = color;
				Rank = rank;
				ColorIsKnown = false;
				PossibleColors = new List<Сolors>();
				PossibleRanks = new List<int>();
				foreach (Сolors c in Enum.GetValues(typeof(Сolors)))
					PossibleColors.Add(c);
				for (int possibleRank = 1; possibleRank <= 5; possibleRank++)
					PossibleRanks.Add (possibleRank);
			}

			public void RemoveExactlyWrongColor(Сolors color)
			{
				PossibleColors.Remove(color);
                if (PossibleColors.Count == 1)
                    KnowColor(PossibleColors[0]);
            }

			public void RemoveExactlyWrongRank(int rank)
			{
				PossibleRanks.Remove (rank);
                if (PossibleRanks.Count == 1)
                    RankKnow(PossibleRanks[0]);
            }

			public void KnowColor(Сolors color)
			{
				PossibleColors.Clear();
				PossibleColors.Add(color);
				ColorIsKnown = true;
			}

			public void RankKnow(int rank)
			{
				PossibleRanks.Clear ();
				PossibleRanks.Add (rank);
				RankIsKnown = true;
			}

            public bool IsCorrectColorForStart(List<Card> cards)
            {
                var setColors = cards.Select(card => card.Color).ToList();
                foreach (var color in PossibleColors )
                    if (setColors.Contains(color)) return false;
                return true;
            }

            public bool IsRightPossibleColor(List<Card> cards)
            {
                List<Сolors> allColors = cards.Select(c => c.Color).ToList();
                List<Сolors> suitableColors = new List<Сolors>();

                foreach (var color in allColors)
                    if (cards.Where(c => c.Color == color).ToList().Select(c => c.Rank).ToList().Max() == Rank - 1)
                        suitableColors.Add(color);

                foreach (var color in PossibleColors)
                    if (!suitableColors.Contains(color)) return false;

                return true;
            }
        }

		private class Player
		{
			private List<Card> Cards;

			public Player()
			{
				Cards = new List<Card>();
			}

			public void AddCard(Card card)
			{
				Cards.Add(card);
			}

			public void DropCard(int cardNumber)
			{
                if (cardNumber >= Cards.Count) throw new Exception("index of card is out of range");
				Cards.RemoveAt(cardNumber);
				Cards.Add(Deck.Pop());
			}

			public Card PlayCard(int cardNumber)
			{
                if (cardNumber >= Cards.Count) throw new Exception("index of card is out of range");
                Card playingCard = Cards[cardNumber];
				Cards.RemoveAt(cardNumber);
				return playingCard;
			}

			public bool GetColorsInfo(Сolors color, List<int> cardNumbers)
            {
                if (!cardNumbers.All(index => index < Cards.Count)) throw new FormatException("index out of range");

				cardNumbers.Sort();

				var cardsOfThisColor = Cards.Select((card, index) => new { newCard = card, newIndex = index }).
					Where(x => x.newCard.Color == color).Select(x => x.newIndex).ToList();

				if (cardsOfThisColor.SequenceEqual(cardNumbers))
				{
					foreach (var index in cardNumbers)
						Cards[index].KnowColor(color);

                    var otherCards = Enumerable.Range(0, Cards.Count).ToList().Except(cardNumbers).ToList();

                    foreach (var index in otherCards)
						Cards[index].RemoveExactlyWrongColor(color);

					return true;
				}
				else
					return false;
			}

			public bool GetRankInfo(int rank, List<int> cardNumbers)
			{
                if (rank < 0 || rank > maxRank) throw new FormatException("rank out of range");
                if (!cardNumbers.All(index => index < Cards.Count)) throw new FormatException("index out of range");

                cardNumbers.Sort();

				var cardsOfThisRank = Cards.Select((card, index) => new { newCard = card, newIndex = index }).
					Where(x => x.newCard.Rank == rank).Select(x => x.newIndex).ToList();

				if (cardsOfThisRank.SequenceEqual(cardNumbers))
				{
					foreach (var index in cardNumbers)
						Cards [index].RankKnow (rank);

					var otherCards = Enumerable.Range (0, Cards.Count).ToList ().Except (cardNumbers).ToList (); 

					foreach (var index in otherCards)
						Cards [index].RemoveExactlyWrongRank (rank);
					return true;
				}
				else
					return false;
			}
		}

		private Player Player1, Player2, CurrentPlayer;

		private Сolors ParseColor(char colorChar)
		{
			switch (colorChar)
			{
				case 'R': return Сolors.Red;
				case 'G': return Сolors.Green;
				case 'B': return Сolors.Blue;
				case 'W': return Сolors.White;
				case 'Y': return Сolors.Yellow;
				default: throw new FormatException("Wrong color char");
			}
		}

		private Card ParseCard(String cardText)
		{
			Сolors cardColor = ParseColor(cardText[0]);
			int cardRank = (int)Char.GetNumericValue(cardText[1]);

            if (cardRank <= 0 || cardRank > maxRank) throw new FormatException("rank out of range");

			return new Card(cardColor, cardRank);
		}

		private Player Opponent(Player currentPlayer)
		{
			if (currentPlayer == null || currentPlayer == Player2)
				return Player1;
			else
				return Player2;
		}

		public void ProcessTurn(string turn)
		{
			turnResult turnRes = ParseTurn(turn);
			switch (turnRes)
			{
				case turnResult.newGame:
					break;
				case turnResult.other:
					TotalTurns++;
					break;
				case turnResult.successPlayedCard:
					TotalTurns++;
					TotoalCards++;
					break;
				case turnResult.riskSuccess:
					TotalTurns++;
					TotoalCards++;
					SuccesRiskCards++;
					break;
				case turnResult.failedRisk:
					TotalTurns++;
					Finished = true;
					break;
				case turnResult.ignoreTurn:
					break;
			}

			CurrentPlayer = Opponent(CurrentPlayer);

			if (CurrentSet.Count == 25 || Deck.Count == 0)
				Finished = true;
		}

		private turnResult ParseTurn(string turn)
		{
			turnResult res = turnResult.ignoreTurn;

			const string startGame = "Start new game with deck ";
			const string playCard = "Play card ";
			const string dropCard = "Drop card ";
			const string tellColor = "Tell color ";
			const string tellRank = "Tell rank ";

			if (turn.IndexOf(startGame) == 0)
			{
				StartNewGame(turn.Substring(startGame.Length).Split(' ').ToList());
				res = turnResult.newGame;
			}
			if (!Finished)
			{
                if (turn.IndexOf(dropCard) == 0)
                {
                    CurrentPlayer.DropCard(int.Parse(turn.Substring(dropCard.Length)));
                    res = turnResult.other;
                }

				if (turn.IndexOf(tellColor) == 0)
				{
					List<String> options = turn.Substring(tellColor.Length).Split(' ').ToList();
					res = Opponent(CurrentPlayer).GetColorsInfo(ParseColor(options[0][0]), options.Skip(3).
                        Select(x => int.Parse(x)).ToList()) ? turnResult.other : turnResult.failedRisk;
				}

				if (turn.IndexOf(tellRank) == 0)
				{
					List<String> options = turn.Substring(tellRank.Length).Split(' ').ToList();
					res = Opponent(CurrentPlayer).GetRankInfo( (int)char.GetNumericValue(options[0][0]), options.Skip(3).
                        Select(x => int.Parse(x)).ToList()) ? turnResult.other : turnResult.failedRisk;
				}

				if (turn.IndexOf(playCard) == 0)
					res = PlayCard(int.Parse(turn.Substring(playCard.Length)));
			}

			return res;
		}

		private turnResult StartNewGame(List<string> textCards)
		{
			if (textCards.Count < 2 * cardsCountPerPlayer + 1) return turnResult.failedRisk;

			Player1 = new Player();
			Player2 = new Player();
			Deck = new Stack<Card>();
			CurrentSet = new List<Card>();
			Finished = false;
			WasStatGet = false;
			TotalTurns = TotoalCards = SuccesRiskCards = 0;
			CurrentPlayer = null;

			List<Card> cardsList = new List<Card>();

			foreach (var card in textCards)
				cardsList.Add(ParseCard(card));

			for (int i = 0; i < cardsCountPerPlayer; i++)
				Player1.AddCard(cardsList[i]);

			for (int i = cardsCountPerPlayer; i < 2 * cardsCountPerPlayer; i++)
				Player2.AddCard(cardsList[i]);

            for (int i = cardsList.Count - 1; i >= 2 * cardsCountPerPlayer; i--)
                Deck.Push(cardsList[i]);

			return turnResult.newGame;
		}

        private turnResult PlayCard(int cardNumber)
		{
			Card playinCard = CurrentPlayer.PlayCard(cardNumber);
            turnResult res;

            if (IsRightSistuation(playinCard))
            {
                if ((playinCard.RankIsKnown && playinCard.Rank == 1 && playinCard.IsCorrectColorForStart(CurrentSet)) ||
                     (playinCard.RankIsKnown && playinCard.Rank > 1 && ExistPreviousCard(playinCard) && playinCard.IsRightPossibleColor(CurrentSet))
                   )
                    res = turnResult.successPlayedCard;
                else
                    res = turnResult.riskSuccess;
                CurrentSet.Add(playinCard);
                CurrentPlayer.AddCard(Deck.Pop());
            }
            else
                res = turnResult.failedRisk;
            return res;
		}

        private bool IsRightSistuation(Card playinCard)
        {
            return
                ( (playinCard.Rank == 1 && !ExistCardsOfColor(playinCard.Color)) ||
                  (playinCard.Rank > 1 && ExistPreviousCard(playinCard))
                );
        }

        private bool ExistCardsOfColor(Сolors color)
		{
			return CurrentSet.Where(c => c.Color == color).ToList().Count > 0;
		}

		private bool ExistPreviousCard(Card card)
		{
			return ( ExistCardsOfColor(card.Color) ) && ( card.Rank - CurrentSet.Where(c => c.Color == card.Color).ToList().Select(c => c.Rank).Max() == 1 );
		}

		public String GetStat()
		{
			WasStatGet = true;
			return string.Format("Turn: {0}, cards: {1}, with risk: {2}", TotalTurns, TotoalCards, SuccesRiskCards);
		}
	}

	class Solve
	{
		public static void Main(string[] args)
		{
			String turn;
			Hanabi hanabi = new Hanabi();
            
			turn = Console.ReadLine();

            while (turn != null)
			{
				hanabi.ProcessTurn(turn);
				if (hanabi.Finished && !hanabi.WasStatGet)
					Console.WriteLine(hanabi.GetStat());
				turn = Console.ReadLine();
			}
		}
	}
}